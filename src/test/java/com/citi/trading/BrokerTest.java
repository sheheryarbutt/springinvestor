package com.citi.trading;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.InvestorTest.MockMarket;

import static org.hamcrest.Matchers.*;

import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = BrokerTest.Config.class)
@DirtiesContext(classMode=ClassMode.BEFORE_EACH_TEST_METHOD)
public class BrokerTest {
	
	@Autowired
	private Broker broker;
	
	@Configuration
	@ComponentScan
	public static class Config {
//		
//		@Bean
//		public Broker broker() {
//			return new Broker();
//		}
//		
		@Bean
		@Scope("prototype")
		public Investor investor() {
			return new Investor();
		}
//		
		@Bean
		public MockMarket mockMarket() {
			return new MockMarket();
		}
		
	

	}
	
	@Test
	public void testGetById() {
		
		assertThat(broker.getByID(2).getBody().getCash(), closeTo(0, 0.0001));
		assertThat(broker.getByID(2).getBody().getID(),equalTo(2));
	}
	
	@Test 
	public void testGetAll() {
		assertThat(broker.getAll().size(), equalTo(3));
	}

	@Test 
	public void testOpenAccount() {
		Investor investor = broker.openAccount(1000);
		assertThat(broker.getByID(4).getBody().getCash(), equalTo(1000.0));
		assertThat(broker.investors.size(), equalTo(4));
	}

	@Test
	public void testCloseAccount() {
		broker.closeAccount(1);
		assertThat(broker.getAll(), hasSize(2));
	}
	
}



