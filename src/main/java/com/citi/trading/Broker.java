package com.citi.trading;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.bind.annotation.RestController;

//@Component cors support added!
@RestController
@CrossOrigin
@RequestMapping("/accounts")
public class Broker {
//	public List<int,Investor> investors;
	//create exception here to maybe throw later
	
	public HashMap<Integer,Investor> investors = new HashMap<>();
	
	public int id=0;
	
	@Autowired
	private ApplicationContext AC;
	
	
	@PostConstruct
	public void PostConstructMethod() {
		Investor investor1 = openAccount(10000);
		Investor investor2 = openAccount(20000);
		Investor investor3 = openAccount(30000);
		
		//investor 1 setup
		investor1.setCash(40000);
		investor1.setPortfolio(new HashMap<>());
		
		//investor 2
		Map<String,Integer> starter2 = new HashMap<>();
		starter2.put("MSFT", 10000);	
		investor2.setCash(0);
		investor2.setPortfolio(starter2);
	
		//investor 3 setup
		Map<String,Integer> starter3 = new HashMap<>();
		starter3.put("MSFT", 10000);
		investor3.setCash(0);
		investor3.setPortfolio(starter3);
	}
	
	@GetMapping( 
	
//			produces={MediaType.APPLICATION_JSON_VALUE}
produces= "application/json"	
)
	public List<Investor> getAll(){
		List<Investor> local_investors = new ArrayList<>();
		investors.forEach((k,v)->local_investors.add(v));
		return local_investors;
	};
	
	
	
	@GetMapping( 
			 value="/{number}",
			produces={MediaType.APPLICATION_JSON_VALUE}
	)
	public ResponseEntity<Investor> getByID(@PathVariable("number") int ID) {
		System.out.println(investors);
		if(investors.get(ID) != null) {
			return new ResponseEntity<Investor>(investors.get(ID),HttpStatus.OK);
		}else {
			return new ResponseEntity<Investor> (HttpStatus.NOT_FOUND);
		}
		
	};
	
	 @RequestMapping
	    (
	        method=RequestMethod.POST,
	        value="/openAccount",
	        produces={MediaType.APPLICATION_JSON_VALUE}
	    )
	 @ResponseStatus(HttpStatus.CREATED)
	public Investor openAccount(@RequestParam("cash") double cash) {
		id+=1;
		System.out.println(id);
//		Investor investor = new Investor();
		System.out.println("here");
		System.out.println("build at:" + new Date());
		Investor investor = AC.getBean(Investor.class);
		
		
		investor.setCash(cash);
		investor.setPortfolio(new HashMap<String,Integer>());
		investor.setID(id);
		investors.put(id,investor);
		
		return investor;
	}
	 @RequestMapping
	    (
	    	value="{ID}",
	        method=RequestMethod.DELETE
	        
	    )
	public ResponseEntity<Investor>  closeAccount(@PathVariable("ID") int ID) {
		
		 if (!investors.containsKey(ID)) return new ResponseEntity<Investor>(HttpStatus.NOT_FOUND);
		 investors.remove(ID);
		return  new ResponseEntity<Investor>(HttpStatus.OK);
			
		
	};
	
}
// What to call in httpad
//-- PORT 8083
//POST /accounts?cash=100 HTTP/1.1


